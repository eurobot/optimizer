#pragma once

#include "constraintbuilder.h"
#include "string.h"

class CarLikeConstraintBuilder : public ConstraintBuilder
{

public:
	CarLikeConstraintBuilder();
	virtual ~CarLikeConstraintBuilder(){}

	/*
	* Initializes the necessary parameters for the creation of the Constraint matrix.
	*/
    void initConstraintBuilder(unsigned int n, std::vector<real_1d_array> qRefList, std::vector<real_1d_array> uRefList, double timeStep, double wheelBase);
    void setTimeStep(double timeStep){this->timeStep = timeStep;}
    void setWheelBase(double wheelBase){this->wheelBase = wheelBase;}

    void setCurrentQ(alglib::real_1d_array Q){QMeas = Q;}
    void setCurrentU(alglib::real_1d_array U){UMeas = U;}
    
private:
	virtual alglib::real_2d_array calculateA(alglib::real_1d_array qRef, alglib::real_1d_array uRef) override;	// q*q dimension matrix
	virtual alglib::real_2d_array calculateB(alglib::real_1d_array qRef, alglib::real_1d_array uRef) override;	// q*u dimension matrix
    virtual alglib::real_1d_array getQMeas() override;											// always returns the last measurement
	virtual alglib::real_1d_array getUMeas() override;											// always returns the last measurement

private:
    alglib::real_1d_array QMeas;
    alglib::real_1d_array UMeas;
    const alglib::real_2d_array mat_A_Default;
    const alglib::real_2d_array mat_B_Default;

protected:
    double wheelBase;
    double timeStep;

    static const char mat_A_Default_String[];
    static const char mat_B_Default_String[];
    static const char vect_Q_Default_String[];
    static const char vect_U_Default_String[];

};
