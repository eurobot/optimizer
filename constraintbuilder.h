#pragma once

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "optimization.h"

using namespace alglib;

struct Constraint
{
	int		uNum;
	double	limit;
	int		cType;
	bool 	absolute;
	Constraint(int u, double l, int c, bool a = false) : uNum(u), limit(l), cType(c), absolute(a) {}
};

/*	PROPER USE OF THIS CLASS
*		1. Reimplement (pure) virtual functions in your derived class.
*		2. After creation call initConstraintBuilder with the appropriate parameters.
*		3. Add your constraints for the u vector with addUConstraint function.
*		4. Call CreateConstraintMatrix which returns the calculated constraint matrix for the given timestep.
		   Don't forget to refresh the values returned by getQMeas() & getUMeas() before calling this function.	
*		5. Call real_1d_array getCTypes after the constraint matrix was created.
*		6. Repeat step 4 and 5 until the end of operation.
*/
class ConstraintBuilder
{
public:
	virtual ~ConstraintBuilder() {};

private:
	virtual real_2d_array calculateA(real_1d_array qRef, real_1d_array uRef) = 0;	// q*q dimension matrix
	virtual real_2d_array calculateB(real_1d_array qRef, real_1d_array uRef) = 0;	// q*u dimension matrix
	virtual real_1d_array getQMeas() = 0;											// always returns the last measurement
	virtual real_1d_array getUMeas() = 0;											// always returns the last measurement
	virtual real_1d_array getQref(unsigned int k);									// k is the num of steps ahead of the current timestep. In case one does not wish to store their QRef and URef  vectors in this class, this function must be implemented.
	virtual real_1d_array getUref(unsigned int k);									// k is the num of steps ahead of the current timestep. In case one does not wish to store their QRef and URef  vectors in this class, this function must be implemented.

private:
	/*! Calculates the q-qref vector in a given timestep, applying the formula below:
	*	(q[k+1]-qref[k+1]) = A[k]*(q[k]-qref[k]) + B[k]*(u[k]-uref[k])
	*	The parameter matrices and vectors have to be coherent. 
	*/
	real_1d_array	calcQHat(real_2d_array A, real_2d_array B, real_1d_array qRef, real_1d_array uRef
							, real_1d_array qMeas, real_1d_array uMeas);

	/* Calculates the number of rows of the constraint matrix.
	*/
	unsigned int calculateRows();

	/* Calculates the number of rows of the constraint matrix.
	*/
	unsigned int calculateCols();

	/* Resets the U parts of the m_UpperBoxConstraints and m_LowerBoxConstraints Vectors to the default values
	*/
	void resetDefaultBoxUConstraints(void);

	/* Calculates and constructs the current m_UpperBoxConstraints and m_LowerBoxConstraints Vectors based on the current state
	*/
	void CalculateBoxUConstraints(void);

	unsigned int				m_N;					// prediction horizon
	unsigned int				m_dimQ;					// dimension of q vector
	unsigned int				m_dimU;					// dimension of u vector
	std::vector<Constraint>		m_UConstraints;			// user defined constraints
	std::vector<Constraint>		m_UBoxConstraints;		// user defined Box constraints
	real_1d_array 				m_UpperBoxConstraints;	//Vector of the Upper box constraints with dimension of N*(q + u)
	real_1d_array 				m_LowerBoxConstraints;	//Vector of the Lower box constraints with dimension of N*(q + u)
	integer_1d_array			m_cTypes;				// constraint types vector
	std::vector<real_1d_array>	m_qRefList;				// list of qRefs
	std::vector<real_1d_array>	m_uRefList;				// list of uRefs
	unsigned long				m_currentTimeStep;		// the current timestep

public:
	/****************
	*	GETTERS		*	
	****************/
	unsigned int	getN();
	unsigned int	getDimQ();
	unsigned int	getDimU();
	real_1d_array&  getUpperBoxConstraints();
	real_1d_array&  getLowerBoxConstraints();


	/*! Returns the cTypes vector.
	*	The values are filled when the Constraints matrix is calculated, so it should be only called after that function.
	*/
	integer_1d_array getCTypes();

	/****************
	*	SETTERS		*
	****************/
	void setN(unsigned int n);
	void setDimQ(unsigned int dimQ);
	void setDimU(unsigned int dimU);
	void setQRefList(std::vector<real_1d_array> qRefList);
	void setUrefList(std::vector<real_1d_array> uRefList);
	void setDefaultBoxConstraints(unsigned int n, unsigned int dimQ, unsigned int dimU);
	/*
	* Initializes the necessary parameters for the creation of the Constraint matrix.
	* @param	n the prediction horizon
	* @param	dimQ the dimension of the q vector
	* @param	dimU the dimension of the u vector
	* @param	qRefList the list of qRef vectors
	* @param	uRefList the list of uRef vectors
	*/
	void initConstraintBuilder(unsigned int n, unsigned int dimQ, unsigned int dimU, std::vector<real_1d_array> qRefList, std::vector<real_1d_array> uRefList);

	/*! Adds a user defined constraint to the constaint list for the specified u value.
	* @param	uIndex the index of the value within the u vector !!!STRARTS FROM 0!!!
	* @param	limit the limit for the value
	* @param	cType the type of the constraint for the value.
	* @NOTE		if cType > 0 ---> abs(value) >= limit
	*			if cType = 0 ---> abs(value)  = limit
	*			if cType < 0 ---> abs(value) <= limit
	*/
	void addUConstraint(unsigned int uIndex, double limit, int cType);

	/*! Adds a user defined box tipe constraint to the constaint list for the specified u value.
	* @param	uIndex the index of the value within the u vector !!!STRARTS FROM 0!!!
	* @param	limit the limit for the value
	* @param	absolute True if the User Constraint is absolute,
	*			meanaing that not the error, but the actual value must pass the constraint.
	*/
	void addBoxUConstraint(unsigned int uIndex, double limit, bool absolute = false);


	/*! Creates and returns the constraint matrix.
	* @param currentTimeStep the current timestep, which is necessary for applying the proper qRef & uRef values.
	* @returns the created constraint matrix
	*/
	real_2d_array CreateConstraintMatrix(unsigned long currentTimeStep);
};