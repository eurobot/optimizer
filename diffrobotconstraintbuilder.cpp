#include "diffrobotconstraintbuilder.h"


const char DiffRobotConstraintBuilder::mat_A_Default_String[] = "[[1,0,0],[0,1,0],[0,0,1]]";
const char DiffRobotConstraintBuilder::mat_B_Default_String[] = "[[0,0],[0,0],[0,0]]";
const char DiffRobotConstraintBuilder::vect_Q_Default_String[] = "[0,0,0]";
const char DiffRobotConstraintBuilder::vect_U_Default_String[] = "[0,0]";

DiffRobotConstraintBuilder::DiffRobotConstraintBuilder() : mat_A_Default(mat_A_Default_String)
														,mat_B_Default(mat_B_Default_String)
														,m_qMeas(vect_Q_Default_String)
														,m_uMeas(vect_U_Default_String)
														,m_timeStep(globalTimeStep)
{
}


DiffRobotConstraintBuilder::DiffRobotConstraintBuilder(double timeStep) : m_timeStep(timeStep)
														,mat_A_Default(mat_A_Default_String)
														,mat_B_Default(mat_B_Default_String)
														, m_qMeas(vect_Q_Default_String)
														, m_uMeas(vect_U_Default_String)
{
}


DiffRobotConstraintBuilder::~DiffRobotConstraintBuilder()
{
}


double DiffRobotConstraintBuilder::getTimeStep()
{
	return m_timeStep;
}


void DiffRobotConstraintBuilder::setTimeStep(double timeStep)
{
	if (timeStep >= 0)
		m_timeStep = timeStep;
}


real_2d_array DiffRobotConstraintBuilder::calculateA(real_1d_array qRef, real_1d_array uRef)
{
	/* MAT_A:
	*   [1		0		-vr(k)*sin(phi_r(k))*T]
	*   [0		1		 vr(k)*cos(phi_r(k))*T]
	*   [0		0			1			      ]
	*/

	real_2d_array mat_A(mat_A_Default);
	double v_r		= uRef[0];
	double phi_r	= qRef[2];

	mat_A[0][2] = (-1.0f) * v_r * (sin(phi_r)) * m_timeStep;
	mat_A[1][2] =  (1.0f) *	v_r * (cos(phi_r)) * m_timeStep;

	return mat_A;
}


real_2d_array DiffRobotConstraintBuilder::calculateB(real_1d_array qRef, real_1d_array uRef)
{
	/* MAT_B:
	*   [T*cos(phi_r(k))		0]
	*   [T*sin(phi_r(k))		0]
	*   [		0				T]
	*/
	
	real_2d_array	mat_B(mat_B_Default);
	double			phi_r = qRef[2];

	mat_B[0][0] = cos(phi_r) * m_timeStep;
	mat_B[1][0] = sin(phi_r) * m_timeStep;
	mat_B[2][1] = m_timeStep;

	return mat_B;
}

alglib::real_1d_array DiffRobotConstraintBuilder::getQMeas()
{
	return m_qMeas;
}

alglib::real_1d_array DiffRobotConstraintBuilder::getUMeas()
{
	return m_uMeas;
}

