#include "carlikeconstraintbuilder.h"
#include <math.h>

using namespace alglib;

const char CarLikeConstraintBuilder::mat_A_Default_String[] = "[[1,0,0],[0,1,0],[0,0,1]]";
const char CarLikeConstraintBuilder::mat_B_Default_String[] = "[[0,0],[0,0],[0,0]]";
const char CarLikeConstraintBuilder::vect_Q_Default_String[] = "[0,0,0]";
const char CarLikeConstraintBuilder::vect_U_Default_String[] = "[0,0]";
CarLikeConstraintBuilder::CarLikeConstraintBuilder():
mat_A_Default(mat_A_Default_String),
mat_B_Default(mat_B_Default_String),
QMeas(vect_Q_Default_String),
UMeas(vect_U_Default_String)
{
}

void CarLikeConstraintBuilder::initConstraintBuilder(unsigned int n, std::vector<real_1d_array> qRefList, std::vector<real_1d_array> uRefList, double timeStep, double wheelBase)
{
    ConstraintBuilder::initConstraintBuilder(n, mat_A_Default.cols(), mat_B_Default.cols(), qRefList, uRefList);
    this->timeStep = timeStep;
    this->wheelBase = wheelBase;
}

real_2d_array CarLikeConstraintBuilder::calculateA(real_1d_array qRef, real_1d_array uRef)
{
    real_2d_array mat_A = mat_A_Default;

    double v = uRef[0];
    double theta = qRef[2];

    /* MAT_A:
    *   [1      0       -v*sin(theta)*T]
    *   [0      1        v*cos(theta)*T]
    *   [0      0        1           ]
    */

    mat_A[0][2] = (-1.0f) * v * sin(theta) * timeStep;
    mat_A[1][2] = v * cos(theta) * timeStep;

    return mat_A;
}

real_2d_array CarLikeConstraintBuilder::calculateB(real_1d_array qRef, real_1d_array uRef)
{
    real_2d_array mat_B = mat_B_Default;

    double v = uRef[0];
    double phi = uRef[1];
    double theta = qRef[2];
    double cosphi = cos(phi);

    /* MAT_B:
    *   [T*cos(theta)      0                   ]
    *   [T*sin(theta)      0                   ]
    *   [T/W*tan(phi)    T*v/W / (cos(phi)^2)]
    */

    mat_B[0][0] = timeStep * cos(theta);
    mat_B[1][0] = timeStep * sin(theta);
    mat_B[2][0] = timeStep / wheelBase  * tan(phi);
    mat_B[2][1] = (timeStep * v ) / wheelBase  / (cosphi * cosphi);

    return mat_B;
}

real_1d_array CarLikeConstraintBuilder::getQMeas()
{
    return QMeas;
}

real_1d_array CarLikeConstraintBuilder::getUMeas()
{
    return UMeas;
}			