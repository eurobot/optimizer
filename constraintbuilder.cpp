#include "constraintbuilder.h"



unsigned int ConstraintBuilder::getN()
{
	return m_N;
}


unsigned int ConstraintBuilder::getDimQ()
{
	return m_dimQ;
}


unsigned int ConstraintBuilder::getDimU()
{
	return m_dimU;
}

real_1d_array&  ConstraintBuilder::getUpperBoxConstraints()
{
	return m_UpperBoxConstraints;
}

real_1d_array&  ConstraintBuilder::getLowerBoxConstraints()
{
	return m_LowerBoxConstraints;
}



real_1d_array ConstraintBuilder::getQref(unsigned int k)
{
	if (k + m_currentTimeStep > m_qRefList.size() - 1)
		return m_qRefList.at(m_qRefList.size() - 1);

	return m_qRefList.at(m_currentTimeStep + k);
}


real_1d_array ConstraintBuilder::getUref(unsigned int k)
{
	if (k + m_currentTimeStep > m_uRefList.size() - 1)
		return m_uRefList.at(m_uRefList.size() - 1);

	return m_uRefList.at(m_currentTimeStep + k);
}


integer_1d_array ConstraintBuilder::getCTypes()
{
	return m_cTypes;
}


void ConstraintBuilder::setN(unsigned int n)
{
	if (n > 0)
		m_N = n;
}

void ConstraintBuilder::setDefaultBoxConstraints(unsigned int n, unsigned int dimQ, unsigned int dimU)
{
	if ((n > 0) && (dimQ > 0) && (dimU > 0))
	{
		unsigned int resultVectorSize = n * (dimQ + dimU);
		m_UpperBoxConstraints.setlength(resultVectorSize);
		m_LowerBoxConstraints.setlength(resultVectorSize);
		for(int i = 0; i < resultVectorSize; i++)
		{
			m_UpperBoxConstraints[i] = INFINITY;
			m_LowerBoxConstraints[i] = - INFINITY;
		}
	}
}

void ConstraintBuilder::resetDefaultBoxUConstraints(void)
{
	for(int i = 0; i < m_N; i++)
	{
		for(int j = 0; j < m_dimU; j++)
		{
			m_UpperBoxConstraints[i*(m_dimQ + m_dimU) + m_dimQ + j] = INFINITY;
			m_LowerBoxConstraints[i*(m_dimQ + m_dimU) + m_dimQ + j] = - INFINITY;
		}
	}
}



void ConstraintBuilder::setDimQ(unsigned int dimQ)
{
	if (dimQ > 0)
		m_dimQ = dimQ;
}


void ConstraintBuilder::setDimU(unsigned int dimU)
{
	if (dimU > 0)
		m_dimU = dimU;
}

void ConstraintBuilder::setQRefList(std::vector<real_1d_array> qRefList)
{
	m_qRefList = qRefList;
}

void ConstraintBuilder::setUrefList(std::vector<real_1d_array> uRefList)
{
	m_uRefList = uRefList;
}

void ConstraintBuilder::initConstraintBuilder(unsigned int n, unsigned int dimQ, unsigned int dimU, std::vector<real_1d_array> qRefList, std::vector<real_1d_array> uRefList)
{
	setN(n);
	setDimQ(dimQ);
	setDimU(dimU);
	setQRefList(qRefList);
	setUrefList(uRefList);
	setDefaultBoxConstraints(n, dimQ, dimU);
}

real_1d_array ConstraintBuilder::calcQHat(real_2d_array A, real_2d_array B, real_1d_array qRef, real_1d_array uRef, real_1d_array qMeas, real_1d_array uMeas)
{
	real_1d_array qHat;
	qHat.setlength(A.rows());

	for (int i = 0; i < A.rows(); i++)
		qHat[i] = 0;

	for (int i = 0; i < A.rows(); i++)
	{
		for (int j = 0; j < A.cols(); j++)
			qHat[i] += A[i][j] * (qMeas[j] - qRef[j]);
		for (int j = 0; j < B.cols(); j++)
			qHat[i] += B[i][j] * (uMeas[j] - uRef[j]);
	}
	return qHat;
}


void ConstraintBuilder::addUConstraint(unsigned int uNum, double limit, int cType)
{
	if (!(uNum >= 0) || !(uNum < m_dimU))
	{
		return;
	}

	Constraint con(uNum, limit, cType);

	for (std::vector<Constraint>::iterator it = m_UConstraints.begin(); it != m_UConstraints.end(); ++it)
	{
		if (it->uNum == con.uNum)
		{
			it->limit = con.limit;
			it->cType = con.cType;
			return;
		}
	}
	m_UConstraints.push_back(con);
}

void ConstraintBuilder::addBoxUConstraint(unsigned int uIndex, double limit, bool absolute)
{
	if (!(uIndex >= 0) || !(uIndex < m_dimU))
	{
		return;
	}

	Constraint con(uIndex, limit, -1, absolute);

	for (std::vector<Constraint>::iterator it = m_UBoxConstraints.begin(); it != m_UBoxConstraints.end(); ++it)
	{
		if ((it->uNum == con.uNum) && (it->absolute == con.absolute))
		{
			it->limit = con.limit;
			it->cType = con.cType;
			return;
		}
	}

	m_UBoxConstraints.push_back(con);
}

void ConstraintBuilder::CalculateBoxUConstraints(void)
{
	resetDefaultBoxUConstraints();
	for (unsigned i = 0; i < m_N; i++)
	{
		real_1d_array uRef = getUref(i);
		for (std::vector<Constraint>::iterator it = m_UBoxConstraints.begin(); it != m_UBoxConstraints.end(); ++it)
		{
			unsigned uNum = it->uNum;
			unsigned uConstraintNum = i * (m_dimQ + m_dimU) + m_dimQ + uNum;
			bool absoulute = it->absolute;
			double limit = it->limit;
			double upperLimit = (absoulute) ? (limit - uRef[uNum]) : (limit);
			double lowerLimit = (absoulute) ? (-limit - uRef[uNum]) : (-limit);

			if(upperLimit < m_UpperBoxConstraints[uConstraintNum])
			{
				m_UpperBoxConstraints[uConstraintNum] = upperLimit;
			}

			if(lowerLimit > m_LowerBoxConstraints[uConstraintNum])
			{
				m_LowerBoxConstraints[uConstraintNum] = lowerLimit;
			}
		}
	}
}


unsigned int ConstraintBuilder::calculateRows()
{
	if (!(m_dimQ > 0) || !(m_dimU > 0) || !(m_N > 0))
	{
		return 0;
	}
	return (m_dimQ * m_N) + (m_UConstraints.size()*2*m_N);	// dimQ * N + (numberOfUserConstraints*2*N)
}


unsigned int ConstraintBuilder::calculateCols()			
{
	if (!(m_dimQ > 0) || !(m_dimU > 0) || !(m_N > 0))
	{
		return 0;
	}
	return ((m_dimQ + m_dimU) * m_N) + 1;	// (dimQ + dimU) * N + 1
}

real_2d_array ConstraintBuilder::CreateConstraintMatrix(unsigned long currentTimeStep)
{
	m_currentTimeStep = currentTimeStep;

	unsigned int rows = calculateRows();
	unsigned int cols = calculateCols();
	real_2d_array ctMatrix;
	std::vector<real_2d_array> aList;
	std::vector<real_2d_array> bList;
	real_1d_array qRef;
	real_1d_array uRef;
	real_1d_array qHat;		// q[k+1]

	// get the necessary qRefs, uRefs, As and Bs
	for (unsigned int i = 0; i < m_N; i++)
	{
		qRef = getQref(i);
		uRef = getUref(i);
		aList.push_back(calculateA(qRef, uRef));
		bList.push_back(calculateB(qRef, uRef));
	}

	// Calculate q[k+1]	 (A[k], B[k], qRef[k], qMeas[k], uRef[k], uMeas[k] should be given)
	qHat = calcQHat(aList.at(0), bList.at(0), getQref(0), getUref(0), getQMeas(), getUMeas());

	//Resize ctMatrix
	ctMatrix.setlength(rows,cols);
	// fill the constraint matrix with zeros
	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++)
			ctMatrix[i][j] = 0;

	// k+1 ... k+N
	for (unsigned int i = 0; i < m_N; i++)
	{
		// 1.1
		// dimQ*dimQ negative identity matrix starting from the position: [i*dimQ][i*(dimQ+dimU)]
		// if i = 0, it should be a dimQ*dimQ poitive identity matrix
		int identityVal = ((i == 0) ? 1 : -1);
		for (unsigned int j = 0; j < m_dimQ; j++)
			ctMatrix[i*m_dimQ + j][i*(m_dimQ + m_dimU) + j] = identityVal;

		// 1.2
		// dimQ*dimQ A[k+i] and dimU*dimQ B[k+i] starting from the position: [i*dimQ][(i-1)*(dimQ+dimU)]
		// if i = 0, there's no A and B in the block
		if (i > 0)
		{
			// rows
			for (unsigned int j = 0; j < m_dimQ; j++)
			{
				// cols for A
				for (unsigned int k = 0; k < m_dimQ; k++)
					ctMatrix[i*m_dimQ + j][(i - 1)*(m_dimQ + m_dimU) + k] = aList.at(i)[j][k];
				// cols for B
				for (unsigned int l = 0; l < m_dimU; l++)
					ctMatrix[i*m_dimQ + j][(i - 1)*(m_dimQ + m_dimU)+ m_dimQ + l] = bList.at(i)[j][l];
			}
		}

		// 1.3
		// last column values (only if i = 0)
		if (i == 0)
		{
			for (unsigned int j = 0; j < m_dimQ; j++)
				ctMatrix[j][cols - 1] = qHat[j];	//qhullam[k+1][j]
		}
	}

	// 1.4	user constraints
	// (2*m_N*m_UConstrints.size())*cols sized matrix with one '1' or -1 for a certain u and a constant in the last column.
	// starts from [N*dimQ][0]
	for (unsigned int j = 0; j < m_UConstraints.size(); j++)
	{
		unsigned int uIndex = m_UConstraints.at(j).uNum;
		for (unsigned int k = 0; k < 2*m_N; k++)
		{
			int value = (k % 2) ? 1 : -1;
			ctMatrix[(m_N*m_dimQ) + (j*m_N * 2) + k][(k / 2) * (m_dimQ + m_dimU) + m_dimQ + uIndex] = value;
			ctMatrix[(m_N*m_dimQ) + (j*m_N * 2) + k][cols - 1] = m_UConstraints.at(j).limit;
		}
	}

	//1.5
	// cType matrix values (should be a 'rows' long real_1d_array)

	m_cTypes.setlength(rows);
	for (unsigned int i = 0; i < rows; i++)
	{
		if (i < (m_N*m_dimQ))
			m_cTypes[i] = 0;	// equality constraints
		else
			m_cTypes[i] = m_UConstraints.at(i - (m_N*m_dimQ)).cType;
	}

	//1.6
	//Box Matrix Creation based on box constraints
	CalculateBoxUConstraints();

	return ctMatrix;
}