
#include "optimizer.h"
#include "ap.h"

using namespace alglib;

Optimizer::Optimizer(unsigned int predictionHorizon, unsigned int singleResultSize):
predictionHorizon(predictionHorizon),
singleResultSize(singleResultSize),
resultVectorSize(singleResultSize * predictionHorizon)
{
    //Create minQP
    minqpcreate(resultVectorSize, optimizationState);
    resultVector.setlength(resultVectorSize);
    for(unsigned i = 0u; i < resultVectorSize; i++)
    {
        resultVector[i] = 0u;
    }
    initOptimizer();
}

#include "math.h"
void Optimizer::initOptimizer(alglib::real_1d_array quadraticTerm, alglib::real_1d_array linearTerm, alglib::real_1d_array scale, double epsg, double epsf, double epsx, int maxits)
{
    setQuadraticTerm(quadraticTerm);
    setLinearTerm(linearTerm);
    setScale(scale);
    setStopParameters(epsg, epsf, epsx, maxits);
}


real_1d_array Optimizer::optimize(real_2d_array& constraints, integer_1d_array& constraintTypes, real_1d_array lowerBoxConstraints, real_1d_array upperBoxConstraints)
{
    minqpreport report;               // Report about the result of the last Optimization

    //Set optional boxconstraints
    if((upperBoxConstraints.length() == resultVectorSize) && (lowerBoxConstraints.length() == resultVectorSize))
    {
        minqpsetbc(optimizationState, lowerBoxConstraints, upperBoxConstraints);
    }
    //Build then set linear constrains
    minqpsetlc(optimizationState, constraints, constraintTypes);

    //Optimize
    minqpoptimize(optimizationState);
    minqpresults(optimizationState, resultVector, report);

    //Prepare and return result of the optimization
    real_1d_array result;
    result.setlength(singleResultSize);

    if(report.terminationtype < 0)
    {
        //Failed optimization
        for(int i = 0; i < singleResultSize; i++)
        {
            result[i] = 0.0f;            //We use the default reference
        }
    }
    else
    {
        //Successful operation
        for(int i = 0; i < singleResultSize; i++)
        {
            result[i] = resultVector[i]; // The result is the first vector.
        }
    }

    return result;
}

/*
*   Setters
*/ 
void Optimizer::setQuadraticTerm(alglib::real_2d_array quadraticTerm)
{
    alglib::real_2d_array quadraticTermLocal;       // Quadratic Term for the cost function
    quadraticTermLocal.setlength(resultVectorSize, resultVectorSize);

    int matSize = quadraticTerm.rows();
    for(int i = 0; i < resultVectorSize; i++)
    {
        for(int j = 0; j < resultVectorSize; j++)
        {
            quadraticTermLocal[i][j] = quadraticTerm[i % matSize][j % matSize];
        }
    }
    minqpsetquadraticterm(optimizationState, quadraticTermLocal);
}


void Optimizer::setQuadraticTerm(alglib::real_1d_array quadraticTerm)
{
    alglib::real_2d_array quadraticTermLocal;       // Quadratic Term for the cost function
    quadraticTermLocal.setlength(resultVectorSize, resultVectorSize);

    int vectSize = quadraticTerm.length();
    for(int i = 0; i < resultVectorSize; i++)
    {
        for(int j = 0; j < resultVectorSize; j++)
        {
            quadraticTermLocal[i][j] = ((i == j) ? (quadraticTerm[i % vectSize]) : (0.0f));
        }
    }
    minqpsetquadraticterm(optimizationState, quadraticTermLocal);
}

void Optimizer::setLinearTerm(alglib::real_1d_array linearTerm)
{
    int vectSize = linearTerm.length();
        
    real_1d_array linearTermLocal;
    linearTermLocal.setlength(resultVectorSize);

    for(int i = 0; i < resultVectorSize; i++)
    {
        linearTermLocal[i] = linearTerm[i % vectSize];
    }
    minqpsetlinearterm(optimizationState, linearTermLocal);
}

void Optimizer::setScale(alglib::real_1d_array scale)
{
    int vectSize = scale.length();

    real_1d_array scaleLocal;
    scaleLocal.setlength(resultVectorSize);

    for(int i = 0; i < resultVectorSize; i++)
    {
        scaleLocal[i] = scale[i % vectSize];
    }
    minqpsetscale(optimizationState, scaleLocal);
}

void Optimizer::setStopParameters(double epsg, double epsf, double epsx, int maxits)
{
    minqpsetalgobleic(optimizationState, epsg, epsf, epsx, maxits);
}

/*
*   Getters
*/
const alglib::real_1d_array& Optimizer::getResultVector(void) const
{
    return resultVector;
}
