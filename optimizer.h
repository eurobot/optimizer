#pragma once

#include "stdafx.h"
#include "optimization.h"

#include "constraintbuilder.h"

/*
* Optimizer class for MPC problem optimalizationw with the help of alglib's quadratic problem solving.
* In order to use: 1.:Create class using the constructor
*                  2.:Call initOptimizer function with the required parameters, and stop conditions.
*                  3.:Periodically call the optimize function with the results of the constraintbuilder.
*/
class Optimizer
{
    public:
    /*! 
    *   Constructors for the optimizer
    */
    Optimizer(unsigned int predictionHorizon, unsigned int singleResultSize);
    
    /*! 
    *   Initializes the optimizer
    *   @param quadraticTerm A vector describing the quadratic terms (a diagaonal matrix made of its element) of the optimizazion, if the cost function is f(x) = 0.5*x'*A*x + b'*x., then A is the quadratic term
    *   @param linearTerm A vector describing the linear terms of the optimizazion, if the cost function is f(x) = 0.5*x'*A*x + b'*x., then b is the linear term
    *   @param scale A vector describing the scale between the variables
    *   @param epsg Stop condition: The  subroutine  finishes  its  work   if   the  condition |v|<EpsG is satisfied
    *   @param epsf Stop condition: The  subroutine  finishes its work if exploratory steepest 
                                    descent  step  on  k+1-th iteration  satisfies   following
                                    condition:  |F(k+1)-F(k)|<=EpsF*max{|F(k)|,|F(k+1)|,1}
    *   @param epsx Stop condition: The  subroutine  finishes  its  work  when  step  length  (with variable scaling being applied) is less than EpsX.
    *   @param maxits Stop condition: The  maximum number of iterations
    *   @note Each of the parameters can be set separately by calling the corresponding setters
    */
    void initOptimizer(alglib::real_1d_array quadraticTerm = "[1.0]", alglib::real_1d_array linearTerm = "[0.0]", alglib::real_1d_array scale = "[1.0]", double epsg = 0.0f, double epsf = 0.0f, double epsx = 0.0f, int maxits = 0);

    /*! 
    *   Calls the constraint Builder, and based on its constraint, optimizes the target function
    */
    real_1d_array optimize(real_2d_array& constraints, integer_1d_array& constraintTypes, real_1d_array lowerBoxConstraints = "[]", real_1d_array upperBoxConstraints = "[]");

    /*
    *   Setters: Sets the parameters of the optimiser.
    *   The parameters where an array is required might be lower in number than the complete resultVector (for example singleResultSize).
    *   In this case the input vector is going to be repeated until the whole taget vector is filled (the size of which is)
    */
    void setQuadraticTerm(alglib::real_2d_array quadraticTerm);
    void setQuadraticTerm(alglib::real_1d_array quadraticTerm); // Sets the quadraticTerm as a diagonal matrix
    void setLinearTerm(alglib::real_1d_array linearTerm);
    void setScale(alglib::real_1d_array scale);
    void setStopParameters(double epsg, double epsf, double epsx, int maxits);

    /*
    *   Getters
    */
    const alglib::real_1d_array& getResultVector(void) const;

    private:
	const unsigned int				    predictionHorizon;    // prediction horizon
    const unsigned int                  singleResultSize;     // The size of a single result
    const unsigned int                  resultVectorSize;     //The intended size of the results vector

    alglib::minqpstate                  optimizationState;    // The state of the optimizer
    alglib::real_1d_array               resultVector;         // The results of the optimizationProces
};