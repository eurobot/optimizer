#include "constraintbuilder.h"
#include "config.h"

class DiffRobotConstraintBuilder : public ConstraintBuilder
{

public:

	DiffRobotConstraintBuilder();
	DiffRobotConstraintBuilder(double timeStep);
	~DiffRobotConstraintBuilder();

	/*! Getter for the sampling time variable.
	* @returns the actually set sampling time in msec.
	*/
	double	getTimeStep();
	
	/*! Setter for the sampling time variable.
	* @param timeStep the sampling time to set in msec.
	*/
	void	setTimeStep(double timeStep);

	/*! Setter for the last measured q vector.
	* @param q the measured q vector.
	* @note this function needs to be called with the latest q value before calling CreateConstraintMatrix!
	*/
	void	setCurrentQ(real_1d_array q) { m_qMeas = q; }
	
	/*! Setter for the last measured u vector.
	* @param u the measured u vector.
	* @note this function needs to be called with the latest u value before calling CreateConstraintMatrix!
	*/
	void	setCurrentU(real_1d_array u) { m_uMeas = u; }

private:

	/*! Calculates and returns the q*q dimension A matrix based on the parameters.
	* @param	qRef the reference q vector in the given timestep
	* @param	uRef the reference u vector in the given timestep
	* @returns	the calculated A matrix
	*/
	virtual real_2d_array calculateA(real_1d_array qRef, real_1d_array uRef) override;	
	
	/*! Calculates and returns the q*u dimension B matrix based on the parameters.
	* @param	qRef the reference q vector in the given timestep
	* @param	uRef the reference u vector in the given timestep
	* @returns	the calculated B matrix
	*/
	virtual real_2d_array calculateB(real_1d_array qRef, real_1d_array uRef) override;

	/*! Returns the last measured q vector
	* @return last measured q vector
	*/
	virtual real_1d_array getQMeas() override;

	/*! Returns the last measured q vector
	* @return last measured u vector
	*/
	virtual real_1d_array getUMeas() override;

	real_1d_array		m_qMeas;				/*!< Member variable for the last measured q vector*/
	real_1d_array		m_uMeas;				/*!< Member variable for the last measured u vector*/
	double				m_timeStep;				/*!< Member variable for the sampling time in msec*/
	const real_2d_array mat_A_Default;			/*!< Member variable for the default value of the A matrix*/
	const real_2d_array mat_B_Default;			/*!< Member variable for the default value of the B matrix*/

protected:
	static const char mat_A_Default_String[];	/*!< Variable with the default value of the A matrix*/
	static const char mat_B_Default_String[];	/*!< Variable with the default value of the B matrix*/
	static const char vect_Q_Default_String[];	/*!< Variable with the default value of the qMeas vector*/
	static const char vect_U_Default_String[];	/*!< Variable with the default value of the uMeas vector*/
};
